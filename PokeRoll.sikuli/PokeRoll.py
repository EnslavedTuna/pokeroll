#rerolling starter
starter = False
#The condition to look for
condition = '(nature or shiny)'
#Change these to the amount of clicks to accept the pokemon
clicks = 12
#What to check for
checkNat1 = True
checkNat2 = True
checkShiny = True
checkMove = False

# STATS: fill in what stat you want minimum, all false = ignore stats
hp20 = True
hp30 = False
att20 = False
att30 = True
def20 = False
def30 = False
spa20 = False
spa30 = False
spd20 = False
spd30 = False
spe20 = False
spe30 = True

## MOVES:      Set move1 to be the move to look for
#move1="1487382549079.png"
move1 = "move1.png"
# NATURES: Uncomment the desired natures
# To add more natures capture the first 3 letters of the desired nature

# Pattern("1487030378571.png").similar(0.85)
# Pattern("1487030584332.png").similar(0.85)
# Pattern("1487076477574.png").similar(0.90)
# Pattern("nature2.png").similar(0.90)
# Pattern("1487251481411.png").similar(0.90)
# Pattern("1487252669475.png").similar(0.85)
# Pattern("1488495957304.png").similar(0.90)

nature1= Pattern("1488495957304.png").similar(0.90)
nature2= Pattern("1487076477574.png").similar(0.90)

# Only change these if you dont run speedengine 5x speed
# Or your computer is slower
delay = 0.17 #delay between button presses, first thing to change if your pc is slow
keydowndelay = 0.06 #how long a button should be pressed
scanDelay = 0.30 #wait for image before scanning
menuChangeDelay = 0.25 #wait for big interface changes such as opening summary

#Dont touch anything beyond this point unless debugging!!
two = Pattern("two.png").similar(0.95)
three= Pattern("1487257937956.png").similar(0.95)
star = "1487024856209.png"
menu ="menu.png"
mainArea = None
natureArea = None
starArea = None
shiny = False
nature = False
stats = False
move = False
app = 'Pokemon Reborn'

hpArea = None
attArea = None
defArea = None
spaArea = None
spdArea = None
speArea = None
moveArea = None
menuArea = None

statsCounter = 0
counter = 0
shinyCounter = 0
natureCounter = 0
moveCounter = 0
pause = False
stop = False

def stopIt(garbage):
    global stop
    stop = True

Env.addHotkey('x',KeyModifier.CTRL,stopIt)   

def pauseIt(garbage):
    global pause
    pause = not pause
   

Env.addHotkey('z',KeyModifier.CTRL,pauseIt)   

def press(letter):
    global pause
    if pause:
        popup('Script paused, click ok to contine','Pause')
        pause = False
        #wait(delay)
    switchApp(app)
    keyDown(letter)
    wait(keydowndelay)
    keyUp(letter)

def findStats():
    global statsCounter
    global stats
    stats = True
    if hp20 and stats:
        stats = findIn(hpArea,two) or findIn(hpArea,three)
    if hp30 and stats:
        stats = findIn(hpArea,three)
    if att20 and stats:
        stats = findIn(attArea,two) or findIn(attArea,three)
    if att30 and stats:
        stats = findIn(attArea,three)
    if def20 and stats:
        stats = findIn(defArea,two) or findIn(defArea,three)
    if def30 and stats:
        stats = findIn(defArea,three)
    if spa20 and stats:
        stats = findIn(spaArea,two) or findIn(spaArea,three)
    if spa30 and stats:
        stats = findIn(spaArea,three)
    if spd20 and stats:
        stats = findIn(spdArea,two)or findIn(spdArea,three)
    if spd30 and stats:
        stats = findIn(spdArea,three)
    if spe20 and stats:
        stats = findIn(speArea,two) or findIn(speArea,three)
    if spe30 and stats:
        stats = findIn(speArea,three)
    if stats:
        statsCounter += 1

def findShiny():
    global shinyCounter
    global shiny
    if checkShiny:
        shiny = False
        if findIn(starArea,star):
            shiny = True
            shinyCounter +=1
    else:
        shiny = True

def findMove():
    global moveCounter
    global move
    move = False
    if checkMove:
        if findIn(moveArea,move1):
            move = True
            moveCounter += 1
    else:
        move = True
    
def findNat():
    global natureCounter
    global nature 
    nature = False
    if checkNat1:
        if findIn(natureArea,nature1):
                nature = True
                natureCounter +=1
    if checkNat2:
        if findIn(natureArea,nature2):
                nature = True
                natureCounter +=1
    if not(checkNat1 or checkNat2):
        nature = True
        

def check():
    while not (findIn(menuArea,menu) or stop):
        press(Key.ESC)
        wait(menuChangeDelay)
    if not starter:    
        press(Key.DOWN)
    wait(delay)
    press(Key.ENTER)
    wait(menuChangeDelay)
    if not starter:
        press(Key.LEFT)
        wait(delay)
        press(Key.LEFT)
        wait(delay)
    press(Key.ENTER)
    wait(delay)
    press(Key.ENTER)
    wait(menuChangeDelay)
    press(Key.RIGHT)
    wait(scanDelay)
    findNat()
    wait(delay)
    press(Key.RIGHT)
    wait(delay)
    press(Key.RIGHT)
    wait(scanDelay)
    findShiny()
    findStats()
    if checkMove:
        press(Key.RIGHT)
        wait(scanDelay)
    findMove()

def getpok():
    for i in range(0,clicks):
        wait(delay)
        press(Key.ENTER)

def findIn(area,image):
    return not(area.exists(image,0) == None)

def getRegions():
    global mainArea
    global natureArea
    global starArea

    global hpArea
    global attArea
    global defArea
    global spaArea
    global spdArea
    global speArea

    global moveArea
    global menuArea
    
    switchApp(app)
    wait(delay)
    press(Key.F12)
    wait(delay)
    mainArea = App.focusedWindow()
    
    natureArea = Region(mainArea)
    natureArea.setX(mainArea.getX()+230)
    natureArea.setY(mainArea.getY()+105)
    natureArea.setW(45)
    natureArea.setH(30) 
    #natureArea.highlight(1)

    starArea = Region(mainArea)
    starArea.setX(mainArea.getX()+2)
    starArea.setY(mainArea.getY()+157)
    starArea.setW(22)
    starArea.setH(22)
    #starArea.highlight(1)

    hpArea = Region(mainArea)
    hpArea.setX(mainArea.getX()+438)
    hpArea.setY(mainArea.getY()+106)
    hpArea.setW(15)
    hpArea.setH(22)
    #hpArea.highlight(2)

    attArea = Region(mainArea)
    attArea.setX(mainArea.getX()+433)
    attArea.setY(mainArea.getY()+150)
    attArea.setW(15)
    attArea.setH(22)
    #attArea.highlight(2)
    
    defArea = Region(mainArea)
    defArea.setX(mainArea.getX()+433)
    defArea.setY(mainArea.getY()+182)
    defArea.setW(15)
    defArea.setH(22)
    #defArea.highlight(2)

    spaArea = Region(mainArea)
    spaArea.setX(mainArea.getX()+433)
    spaArea.setY(mainArea.getY()+214)
    spaArea.setW(15)
    spaArea.setH(22)
    #spaArea.highlight(2)

    spdArea = Region(mainArea)
    spdArea.setX(mainArea.getX()+433)
    spdArea.setY(mainArea.getY()+246)
    spdArea.setW(15)
    spdArea.setH(22)
    #spdArea.highlight(2)

    speArea = Region(mainArea)
    speArea.setX(mainArea.getX()+433)
    speArea.setY(mainArea.getY()+278)
    speArea.setW(15)
    speArea.setH(22)
    #speArea.highlight(2)

    moveArea = Region(mainArea)
    moveArea.setX(mainArea.getX()+315)
    moveArea.setY(mainArea.getY()+320)
    moveArea.setW(80)
    moveArea.setH(25)
    #moveArea.highlight(2)

    menuArea = Region(mainArea)
    menuArea.setX(mainArea.getX()+375)
    menuArea.setY(mainArea.getY()+40)
    menuArea.setW(100)
    menuArea.setH(80)
    #menuArea.highlight(2)


getRegions()
while not eval(condition):
    counter += 1
    switchApp(app)
    wait(delay)
    press(Key.F12) 
    wait(delay)
    press(Key.ALT)
    getpok()
    wait(delay)
    check()
    if stop:
        break;
    
popup('Found in '+str(counter)+ ' tries\nPassed: \n' + str(natureCounter)+' correct natures\n' + str(shinyCounter) + ' shinies' + ' \n' + str(statsCounter)+ ' Desired stats\n' + str(moveCounter) + ' Desired Eggmove' ,'Gotcha!')











